import page from "//unpkg.com/page/page.mjs";
import { updateCameraView } from "./aframe.js"

/* utils */
const $router = document.querySelector(`ls-interface-panel[role="reader"]`);

const routesCameraViews = {
	'/': { position: '0 1.6 0', rotation: '0 0 0' },
	"/releases": { position: '0 15 -15', rotation: '0 -30 0', dur: 2000 },
	"/links": { position: '0 10 30', rotation: '-30 -180 0', dur: 3000 },
	/* "/example1": { position: '-70 1.6 0', rotation: '0 -90 0' }, */
	/* "/example2": { position: '5 20 0', rotation: '-45 0 0' }, */
}

function handleRoute(ctx) {
	const $previousRoute = $router.querySelector('ls-reader[is-visible]')
	$previousRoute?.removeAttribute("is-visible")

	updateCameraView(routesCameraViews[ctx.canonicalPath])
	const $route = document.querySelector(`ls-reader[route="${ctx.canonicalPath}"]`);
	$route.setAttribute("is-visible", true)
}


page('/', handleRoute);
page('/releases', handleRoute);
page('/links', handleRoute);

/* initial load ? */
page();
updateCameraView(routesCameraViews[window.location.pathname])
