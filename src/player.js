customElements.define(
	"radio4000-player-stop",
	class extends HTMLButtonElement {
		connectedCallback() {
			this.addEventListener("click", this.stopPlayer.bind(this));
		}
		stopPlayer() {
			document.querySelector('body').removeAttribute('is-player')
			document.querySelector('radio4000-player').remove()
			/* create anew player; hackz */
			initPlayer()
		}
	},
	{ extends: "button" }
);


const initPlayer = function () {
	/* let's catch when track change, so we can do stuff */
	const $playerContainer = document.querySelector('ls-interface-panel[role="player"]')
	const $player = document.createElement('radio4000-player')
	$player.addEventListener('trackChanged', (event) => {
		const eventData = event.detail[0]
		console.info('trackChanged event', eventData)
		/* it is the first track */
		if (
			!eventData ||
			eventData.previousTrack ||
			eventData.previousTrack.id
		) {
			document.querySelector('body').setAttribute('is-player', true)
		}
	})
	const $stop = document.createElement('button', {
		is: "radio4000-player-stop"
	})
	$stop.textContent = 'x'
	$stop.setAttribute("is", "radio4000-player-stop")
	$playerContainer.replaceChildren($stop, $player)
}

initPlayer()
